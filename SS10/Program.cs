﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS10
{
    class Program
    {
        static void Main(string[] args)
        {
            Datenpool<int> pool = new Datenpool<int>();
            pool.Add(3);
            pool.Add(4);
            pool.Add(5);
            Datenpool<int>.FktDelegate quad = delegate (int a) { return a *= a; };
            pool.ForAll(quad);
            foreach(Datenpool<int>.Node shit in pool)
            {
                Console.WriteLine(shit.Payload);
            }
        }

    }

    class Datenpool<T> : IEnumerable
    {
        public delegate T FktDelegate(T a);

        public class Node
        {
            public T Payload { get; set; }
            public Node Next { get; set; }
            
            public Node(T payload)
            {
                Payload = payload;
            }
        }

        Node head;

        public void Add(T daten)
        {
            Node insertion = new Node(daten);
            insertion.Next = head;
            head = insertion;
        }

        public void ForAll(FktDelegate f)
        {
            foreach(Node node in this)
            {
                node.Payload = f(node.Payload);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            Node pointer = head;
            while (pointer != null)
            {
                yield return pointer;
                pointer = pointer.Next;
            }
        }
    }

    
}
