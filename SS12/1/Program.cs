﻿namespace Prog2_WAufgabe33
{
    class Program
    {
        static void Main(string[] args)
        {
            Lotto lotto = new Lotto();
            Spieler a = new Spieler("Spieler 1", 1, 2, 44, 84, 48, 6);
            Spieler b = new Spieler("Spieler 2", 4, 85, 96, 45, 87, 32);
            lotto.RegistriereSpieler(a);
            lotto.RegistriereSpieler(b);
            
            lotto.StarteSpiel();
            

        }
    }
}
