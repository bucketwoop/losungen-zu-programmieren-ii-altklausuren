﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog2_WAufgabe33
{
    class Spieler
    {
        public string Name { get; private set; }
        int[] tippZahlen;

        public Spieler(string _name, params int[] _tippZahlen)
        {
            foreach (int z in _tippZahlen)
            {
                if(z < 0 || z > 100)
                {
                    // throw new NumberNotValidException();
                }
            }

            Name = _name;
            tippZahlen = _tippZahlen;
        }

        public void PruefeZahlen(int[] _zahlen)
        {
            bool gewonnen = true;

            for (int i = 0; i < _zahlen.Length; i++)
            {
                if (tippZahlen[i] != _zahlen[i])
                    gewonnen = false;
            }

            if(gewonnen)
                Console.WriteLine(Name + " hat gewonnen!");
        }
    }
}
