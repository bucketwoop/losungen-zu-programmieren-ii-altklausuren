﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog2_WAufgabe33
{
    public delegate void ZiehungDel(int[] _gezogen);

    class Lotto
    {
        ZiehungDel Ziehung;

        public void RegistriereSpieler(Spieler _neuerSpieler)
        {
            Ziehung += _neuerSpieler.PruefeZahlen;
        }

        public void StarteSpiel()
        {
            Random rand = new Random();
            int[] gezogen = new int[6];
            for (int i = 0; i < gezogen.Length; i++)
            {
                gezogen[i] = rand.Next(1, 101);
            }
            Ziehung(gezogen);
        }
    }
}
