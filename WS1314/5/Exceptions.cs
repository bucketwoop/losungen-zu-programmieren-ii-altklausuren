﻿using System;

namespace WS1314._5
{
    public class Exceptions
    {
        public static void A5()
        {
            try
            {
                Console.Write("A");
                Object o = null;
                long x = 0;
                Console.Write(o.ToString());
                long y = 3 / x;
                Console.Write("B");
            }
            catch (DivideByZeroException e)
            {
                Console.Write("C");
            }
            catch (NullReferenceException e)
            {
                Console.Write("D");
            }
            catch (Exception e)
            {
                Console.Write("E");
            }
            finally
            {
                Console.Write("F");
            }
        }
    }
}