﻿using System.ComponentModel;

namespace WS1314._4
{
    public class OperatorOverloading
    {
        
    }
    class Komplex
    {
        private double real;
        private double imaginär;
        public Komplex(double real = 0, double imaginär = 0)
        {
            this.real = real;
            this.imaginär = imaginär;
        }
        // ---> Position A in der Klasse Komplex <---
        public static implicit operator Komplex(double x)
        {
            return new Komplex(x, 0);
        }

        public static Komplex operator +(Komplex a, Komplex b)
        {
            return new Komplex(a.real+b.real, a.imaginär+b.imaginär);
        }
    }

    class Program
    {
        public static void A4()
        {
            Komplex x = 5.4; // ein Operator wird benötigt
            Komplex y = new Komplex(3.5, 7.9);
            x += y; // ein Operator wird benötigt
        }
    }
}