﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS13._3
{
    class Vererbung
    {
        public static void A3()
        {
            A31();
            Console.Write("Aufgabe 3.2: ");
            Console.Write(new C1.C2().i);
            TestApp.A33();
            Console.WriteLine("Aufgabe 3.5: ");
            Test2.A35();
        }

        private static void A31()
        {
            Console.Write("Aufgabe 3.1: ");
            try
            {
                int x = 0;
                Object o = null;
                Console.Write("0");
                Console.Write(o.ToString());
                int y = 3 / x;
                Console.Write("1");
            }
            catch (DivideByZeroException e)
            {
                Console.Write("2");
            }
            catch (NullReferenceException e)
            {
                Console.Write("3");
            }
            catch (Exception e)
            {
                Console.Write("4");
            }
            finally
            {
                Console.Write("5");
            }
            Console.Write("6\n");
        }

        /* 3.2 */
        class C1
        {
            public int i = 1;
            public class C2
            {
                public int i;
                public C2() { i = 2; }
            }
        }

        /* 3.3. */
        class C2
        {
            public C2() { Console.Write("A"); }
            public static void m() { Console.Write("C"); }
        }

        class C3 : C2
        {
            public C3() { Console.Write("D"); }
        }
        public class TestApp
        {
            public static void A33()
            {
                Console.Write("\nAufgabe 3.3: ");
                C2.m();
                C3 v1 = new C3();
                Console.WriteLine();
            }
        }

        /* 3.4 */
        class Test
        {
            static void A34()
            {
                C5 myC5 = new C5(3.14);
            }
        }

        class C4
        {
            public C4(double d)
            {
            }
        }
        class C5 : C4
        {
            public C5(double d) /*>*/ : base(d) /*<*/
            {
            }
        }

        /* 3.5 */

        class Test2
        {
            public static void A35()
            {
                A instanz1 = new B();
                instanz1.F(); /* B.F */
                instanz1.G(); /* A.G */
                instanz1.H(); /* A.H */
                B instanz2 = new C();
                instanz2.F(); /* C.F */
                instanz2.G(); /* C.G */
                instanz2.H(); /* B.H */
                A instanz3 = new C();
                instanz3.F(); /* C.F */
                instanz3.G(); /* A.G */
                instanz3.H(); /* A.H */
            }
        }
        class A
        {
            public virtual void F() { Console.Write("A.F "); }
            public void G() { Console.Write("A.G "); }
            public virtual void H() { Console.Write("A.H "); }
        }
        class B : A
        {
            public override void F() { Console.Write("B.F "); }
            public new virtual void G() { Console.Write("B.G "); }
            public new void H() { Console.Write("B.H "); }
        }
        class C : B
        {
            public override void F() { Console.Write("C.F "); }
            public override void G() { Console.Write("C.G "); }
            public new virtual void H() { Console.Write("C.H "); }
        }
        
        /* 3.6 */
        interface I1
        {
            void test();
        }

        interface I2 : I1
        {
            void test2();
        }

        abstract class C6
        {
            public abstract void test3();

            int test4()
            {
                return(4);
            }
        }

        class C7 : C6, I2
        {
            public override void test3()
            {
                
            }
            public void test()
            {
                
            }

            public void test2()
            {
                
            }
        }
    }
}
