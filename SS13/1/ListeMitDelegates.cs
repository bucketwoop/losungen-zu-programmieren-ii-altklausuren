﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace SS13._1
{
    class ListeMitDelegates
    {
        public class Program
        {
            public static void A1()
            {
                Boerse boerse = new Boerse();
                Broker b1 = new Broker();
                Broker b2 = new Broker();
                boerse.Abonniere("SAP AG", b1.NeuerKurs); // Die Broker registrieren
                boerse.Abonniere("BMW AG", b1.NeuerKurs); // sich für einzelne Aktien
                boerse.Abonniere("Linde AG", b2.NeuerKurs);
                boerse.Abonniere("SAP AG", b2.NeuerKurs);
                boerse.Kursaktualisierung("SAP AG", 58.25); // Die Kursänderung soll an die
                                                            // gespeicherten NeuerKurs()-Methoden von b1 und b2 weitergegeben werden
            }
        }
        public class Broker
        {
            public void NeuerKurs(string name, double kurs)
            {
                Console.WriteLine($"{name}: {kurs}");
            }
        }
        class Boerse
        {
            public delegate void Abonnenten(string name, double kurs);

            private Aktie first;

            public class Aktie
            {
                public string Name { get; }
                public Abonnenten Abonnenten { get; set; }
                public Aktie Next { get; set; }

                public Aktie(string name, Abonnenten abonnent)
                {
                    Name = name;
                    Abonnenten = abonnent;
                }
            }

            public void Abonniere(string aktienName, Abonnenten abonnent)
            {
                if (first == null)
                {
                    first = new Aktie(aktienName, abonnent);
                    return;
                }

                Aktie match = FindByName(aktienName);

                if (match == null)
                {
                    Aktie newfirst = new Aktie(aktienName, abonnent);
                    newfirst.Next = first;
                    first = newfirst;
                }
                else
                {
                    match.Abonnenten += abonnent;
                }
            }

            public void Kursaktualisierung(string aktienname, double kurswert)
            {
                Aktie match = FindByName(aktienname);
                if (match == null) throw new InvalidDataException();
                else
                {
                    match.Abonnenten(aktienname, kurswert);
                }
            }

            private Aktie FindByName(string name)
            {
                Aktie pointer = first;
                while(pointer != null)
                {
                    if (pointer.Name == name)
                    {
                        return pointer;
                    }
                    pointer = pointer.Next;
                }
                return null;
            }
        }
    }
}
