﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1516._4
{
    class Delegates
    {
        public delegate bool Filter(String s);
        public static List<String> filter(List<String> data, Filter filter)
        {
            List<String> result = new List<String>();
            foreach (String value in data)
            {
                if (filter(value))
                {
                    result.Add(value);
                }
            }
            return result;
        }

        public static void A4()
        {
            Console.WriteLine("(a)");
            List<String> a = new List<String>();
            a.Add("Aasgeier");
            a.Add("Asslack");
            a.Add("The Bastard John Snow");
            a.Add("kurz");

            PrintList(a);
            /* list nur Strings enthält, die mit einem A beginnen: */
            List<String> list1 = filter(a, val =>
            {
                if (val[0] == 'A')
                    return true;
                else
                    return false;
            });
            PrintList(list1);
            /* list nur Strings enthält, die länger als 5 Zeichen sind: */
            List<String> list2 = filter(a, val =>
            {
                if (val.Length > 5)
                    return true;
                else
                    return false;
            });
            PrintList(list2);
            /* list alle Elemente enthält, die auch in a enthalten sind: */
            List<String> list3 = filter(a, val => true);
            PrintList(list3);
            Main2();
        }

        public static void PrintList(List<String> list)
        {
            Console.WriteLine();
            foreach (string s in list) Console.WriteLine(s);
        }

        /* b */
        delegate int NeuerDelegate(String a, String b);

        /* c */
        public delegate String A(String x);
        public delegate String B(String x, A a);
        public static String method(String x)
        {
            return x.Replace ("a", "e");
        }
        public static String method2(String x)
        {
            return x.Remove (x.Length - 1);
        }
        public static String method3(String x, A a)
        {
            return a (x);
        }

        public static void Main2()
        {
            Console.WriteLine("(b)");
            A a = method;
            String ergebnis = a(a(a("HalloWelt")));
            Console.WriteLine(ergebnis);
            /* HelloWelt */

            a = method2;
            ergebnis = a(a(a("HalloWelt")));
            Console.WriteLine(ergebnis);
            /* HalloW */

            B b = method3;
            ergebnis = b("HalloWelt", method2);
            Console.WriteLine(ergebnis);
            /* HalloWel */
        }

    }
}
