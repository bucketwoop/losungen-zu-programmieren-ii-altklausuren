﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1516._3
{
    interface IMetricDimensions
    {
        string ShowDimensions();
    }

    interface IEnglishDimensions
    {
        string ShowDimensions();
    }

    class Box : IMetricDimensions
    {
        public double Length { get; private set; }
        public double Width { get; private set; }
        public Box(double Length, double Width)
        {
            this.Length = Length;
            this.Width = Width;
        }
        public string ShowDimensions()
        {
            return string.Format("Box : {0}m x {1}m", Length, Width);
        }
    }

    class Container : Box, IEnglishDimensions
    {
        public Container(double Length, double Width) : base(Length, Width) { }

        string IEnglishDimensions.ShowDimensions()
        {
            return string.Format($"{Length*3} Fuss x {Width*3}Fuss");
        }
        public new string ShowDimensions()
        {
            return string.Format("Container : {0}m x {1}m", Length, Width);
        }
    }

    class Program
    {
        static void english(IEnglishDimensions inEng)
        {
            Console.WriteLine(inEng.ShowDimensions());
        }
        public static void A3()
        {
            Container c = new Container(10, 3);
            Box b = c;
            Console.WriteLine(c.ShowDimensions());
            english(c);
            Console.WriteLine(b.ShowDimensions());
            if (b is IEnglishDimensions)
                english(b as IEnglishDimensions);
        }
    }
}
