﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1516._2
{
    class Stack<T>
    {
        class Element
        {
            public T Data;
            public Element Next;
            public Element(T Data, Element Next)
            {
                this.Data = Data;
                this.Next = Next;
            }

            public static Element operator +(Element e, int i)
            {
                while(e != null && i != 0)
                {
                    e = e.Next;
                    i--;
                }
                if(e == null)
                {
                    throw new ArgumentOutOfRangeException();
                }
                return e;
            }

            public static Element operator ++(Element e) => e.Next;
        }

        private Element top = null;

        public void Push (T Value )
        {
            this.top = new Element(Value, this.top);
        }

        public void Print()
        {
            for(Element x = this.top ; x != null; x++)
{
                Console.WriteLine(x.Data);
            }
        }
        public T this[int index]
        {
            get { return(this.top + index).Data; }
        }
    }
}
