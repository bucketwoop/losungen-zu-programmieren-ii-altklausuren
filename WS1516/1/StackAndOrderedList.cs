﻿using System;
using System.Collections;

namespace WS1516._1
{
    partial class Program
    {
        public class StackAndOrderedList<T> where T : class, IComparable
        {
            private class Node
            {
                public T Payload { get; set; }
                public Node Next { get; set; }

                public Node(T payload)
                {
                    Payload = payload;
                }
            }

            Node _headOfStack;
            Node _headOfOrderedList;

            public void Add(T item)
            {
                AddToStack(item);
                AddToOrderedList(item);
            }

            private void AddToOrderedList(T item)
            {
                Node insertion = new Node(item);
                if (_headOfOrderedList == null)
                {
                    _headOfOrderedList = insertion;
                    return;
                }
                Node pointer = _headOfOrderedList;
                Node drag = null;
                while(pointer != null)
                {
                    if (pointer.Payload.CompareTo(insertion.Payload) > 0)
                    {
                        if(drag == null)
                        {
                            insertion.Next = pointer;
                            _headOfOrderedList = insertion;
                        }
                        else
                        {
                            insertion.Next = drag.Next;
                            drag.Next = insertion;
                        }
                        return;
                    }
                    drag = pointer;
                    pointer = pointer.Next;
                }
                drag.Next = insertion;
            }

            private void AddToStack(T item)
            {
                Node insertion = new Node(item);
                insertion.Next = _headOfStack;

                /******************************* 
                Alternative:
                Node insertion = new Node(item)
                {
                    Next = _headOfStack
                };
                *******************************/

                _headOfStack = insertion;
            }

            public T this[int i]
            {
                get
                {
                    Node pointer = _headOfStack;
                    int j = 0;
                    while(pointer != null)
                    {
                        if (i == j) return pointer.Payload;
                        j++;
                        pointer = pointer.Next;
                    }
                    throw new IndexOutOfRangeException();
                }
            }

            public IEnumerable GetStackEnumerator()
            {
                return ListWalker(_headOfStack);
            }

            public IEnumerable GetSortedEnumerator()
            {
                return ListWalker(_headOfOrderedList);
            }

            private IEnumerable ListWalker(Node start)
            {
                Node pointer = start;
                while (pointer != null)
                {
                    yield return pointer.Payload;
                    pointer = pointer.Next;
                }
            }

            /* !!! Nicht von der Aufgabe gefordert !!! */
                public void PrintLists()
            {
                Console.WriteLine("Stack:");
                foreach (string s in GetStackEnumerator())
                    Console.WriteLine(s);

                Console.WriteLine("Sorted List:");
                foreach (string s in GetSortedEnumerator())
                    Console.WriteLine(s);
            }
        }

    }
}
