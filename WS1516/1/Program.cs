﻿using System;
/* !!! Nicht von der Aufgabe gefordert !!! */
namespace WS1516._1
{
    partial class Program
    {
        public static void A1()
        {
            StackAndOrderedList<string> liste = new StackAndOrderedList<string>();

            liste.Add("Gabi");
            liste.Add("Hans");
            liste.Add("Anton");
            liste.Add("Lisa");

            liste.PrintLists();

            Console.WriteLine($"Drittes Element auf dem Stapel: {liste[2]}");
            try
            {
                Console.WriteLine($"Fünftes Element auf dem Stapel: {liste[4]}");
            }
            catch(IndexOutOfRangeException)
            {
                Console.WriteLine($"OutOfRange Exception! Es gibt kein fünftes element ;)!");
            }

        }

    }
}
