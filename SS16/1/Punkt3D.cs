﻿using System;

namespace SommerSemester16._1
{
    class Punkt3D : Punkt2D
    {
        int z;
        public Punkt3D(int x, int y, int z) : base(x, y)
        {
            this.z = z;
        }
        public static Punkt3D operator +(Punkt3D p, int summand)
        {
            Console.Write("D ");
            return new Punkt3D(p.x + summand, p.y + summand, p.z + summand);
        }
        public static bool operator !(Punkt3D p)
        {
            return true;
        }
    }
}