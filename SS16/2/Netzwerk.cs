﻿namespace SommerSemester16._2
{
    class Netzwerk<A> where A: Named, new()
    {
        class Knoten
        {
            public A Daten;
            public Knoten Nächster;
            public Liste<Knoten> Verbindungen;
        }

        Knoten start;

        public void Hinzufügen(A einObjekt)
        {
            // ******************
            // Fehlende Methode 1
            // ******************
            Knoten einfügung = new Knoten();
            einfügung.Daten = einObjekt;
            einfügung.Nächster = start;
            start = einfügung;
        }
        public void VerbindungHinzufügen(string erster, string zweiter)
        {
            // ********************************
            // Fehlende Methode 2 - Lösung naiv
            // ********************************
            Knoten zeigerA = start;
            while(zeigerA != null)
            {
                if(zeigerA.Daten.Name == erster)
                {
                    Knoten zeigerB = start;
                    while(zeigerB != null)
                    {
                        if(zeigerB.Daten.Name == zweiter)
                        {
                            zeigerA.Verbindungen.Hinzufügen(zeigerB);
                            return;
                        }
                        zeigerB = zeigerB.Nächster;
                    }
                }
                zeigerA = zeigerA.Nächster;
            }
            
            // *************************************
            // Fehlende Methode 2 - Lösung erweitert
            // *************************************
            /*
            Knoten zeigerA = start;
            while(zeigerA != null)
            {
                Knoten refA = FindeReferenzZuName(erster);
                Knoten refB = FindeReferenzZuName(zweiter);
                if (refA != null && refB != null)
                {
                    refA.Verbindungen.Hinzufügen(refB);
                }
            }
        }

        private Knoten FindeReferenzZuName(string name)
        {
            Knoten zeiger = start;
            while(zeiger != null)
            {
                if(zeiger.Daten.Name == name)
                {
                    return zeiger;
                }
                zeiger = zeiger.Nächster;
            }
            return null;
        }
        */
        }
    }
}