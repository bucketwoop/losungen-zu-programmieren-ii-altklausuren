﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1213._1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Car> cars = new List<Car>();

            // Filter um alle Fahrzeuge zu ermitteln, die mehr als 1000 Kosten
            // als Lambda-Ausdruck
            List<Car> filtered1 = cars.FindAll(c => c.Price > 1000);
            // als Anonyme Methode
            List<Car> filtered2 = cars.FindAll(delegate (Car c) { return c.Price > 1000; });

            // Filter um alle Fahrzeuge zu ermitteln, die mehr 500/Monat bei 12 Monatsraten kosten
            // als Lambda-Ausdruck
            List<Car> filtered3 = cars.FindAll(c => c.Price > 5 * 12);
            // als Anonyme Methode
            List<Car> filtered4 = cars.FindAll(delegate (Car c) { return c.Price > 5 * 12; });
        }
    }
}
