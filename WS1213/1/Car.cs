﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1213._1
{
    class Car
    {
        public String Type { get; set; }
        public int MaxSpeed { get; set; }
        public int Price { get; set; }
        public Car(String type, int speed, int price)
        {
            Type = type;
            MaxSpeed = speed;
            Price = price;
        }
    }
}
