﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1213._2
{
    class Karte
    {
        public int Wert { get; }
        static Random rand = new Random();

        public Karte ()
        {
            Wert = rand.Next(1, 12);
        }
    }
}
