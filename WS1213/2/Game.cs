﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WS1213._2
{
    class Game
    {
        class LElem
        {
            public Player player;
            public bool verloren = false;
            public LElem next;
            public LElem (Player player)
            {
                this.player = player;
            }
        }

        LElem root;
        int freiePlaetze = 5;

        public void AddPlayer(Player player)
        {
            if (player.Alter >= 18 || freiePlaetze > 0)
            {
                if (root != null)
                {
                    LElem curr = root;
                    while (curr.next != null)
                        curr = curr.next;
                    curr.next = new LElem(player);
                }
                else
                    root = new LElem(player);
                freiePlaetze--;
            }
            else
                throw new ArgumentOutOfRangeException();
        }

        public void Start()
        {
            LElem curr = root;
            int gezogeneKarten = 0;

            do
            {
                gezogeneKarten = 0;

                // Neue Runde startet
                while (curr != null)
                {
                    // Fragen ob Spieler neue Karte möchte und noch nicht verloren hat
                    if (curr.player.AnotherCard() && curr.player.Punktezahl <= 21)
                    {
                        // möchte noch eine Karte
                        try
                        {
                            // Karte ausgeben
                            curr.player.TakeCard(new Karte());

                            // Keine Exception => Karte wurde gezogen!
                            gezogeneKarten++;
                        }
                        catch
                        {
                            // Exception => Spieler hat schon verloren,
                            // keine Karte gezogen worden
                        }
                    }
                    curr = curr.next;
                }
            } while (gezogeneKarten > 0);
            
            // Spiel zu Ende => Gewinner suchen
            Player gewinner = null;
            int maxPunktzahl = 0;

            // Gewinnende Punktzahl herausfinden
            curr = root;
            while(curr != null)
            {
                if (curr.player.Punktezahl > maxPunktzahl && curr.player.Punktezahl <= 21)
                {
                    gewinner = curr.player;
                    maxPunktzahl = gewinner.Punktezahl;
                }
                curr = curr.next;
            }

            // Mehrere Gewinner?
            int count = 0;
            curr = root;
            while(curr != null)
            {
                if (curr.player.Punktezahl == maxPunktzahl)
                    count++;
                curr = curr.next;
            }

            if (count > 1)
                Console.WriteLine("unentschieden");
            else
                Console.WriteLine(gewinner.Name + ": " + gewinner.Punktezahl);
        }
    }
}
