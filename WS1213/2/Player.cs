﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1213._2
{
    abstract class Player
    {
        protected static Random rand = new Random();
        public string Name { get; }
        public int Alter { get; }
        public int Punktezahl { get; set; }

        public Player(string name, int alter)
        {
            Name = name;
            Alter = alter;
            Punktezahl = 0;
        }

        public bool TakeCard(Karte karte)
        {
            if (Punktezahl > 21)
                throw new Exception();

            Punktezahl += karte.Wert;
            return Punktezahl <= 21;
        }

        public abstract bool AnotherCard();
    }

    class DumpPlayer : Player
    {
        public DumpPlayer(string name, int alter) : base(name, alter) { }

        public override bool AnotherCard() => true;
    }

    class BetterPlayer : Player
    {
        public BetterPlayer(string name, int alter) : base(name, alter) { }

        public override bool AnotherCard() => Punktezahl < 15;
    }

    class DrunkPlayer : Player
    {
        public DrunkPlayer(string name, int alter) : base(name, alter) { }

        public override bool AnotherCard() => rand.Next(0,2) == 1;
    }
}