﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS1213._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Player a = new DumpPlayer("chuck", 66);
            Player b = new DumpPlayer("bud", 55);
            Player c = new BetterPlayer("bruce", 77);
            Player d = new DrunkPlayer("don", 77);
            Game game = new Game();
            game.AddPlayer(a);
            game.AddPlayer(b);
            game.AddPlayer(c);
            game.AddPlayer(d);
            game.Start();
        }
    }
}
