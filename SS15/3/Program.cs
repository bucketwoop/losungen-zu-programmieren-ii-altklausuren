﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS15._3
{
    abstract class Bike
    {
        public String Manufacturer { get; private set; }

        public int CalculateSize(double lengthOfLeg)
        {
            return (int)(0.226 * lengthOfLeg);
        }

        public Bike(string manufacturer)
        {
            Manufacturer = manufacturer;
        }
    }

    class ElektroBike : Bike
    {
        public ElektroBike() : base("elektron")
        { }
        public new String CalculateSize(double lengthOfLeg)
        {
            return "Der/Die Fahrer/in hat lange Beine.";
        }
    }
    class MountainBike : Bike
    {
        public Boolean HasSuspension { get; set; }
        public MountainBike(Boolean s, String manufacturer) : base(manufacturer)
        {
            HasSuspension = s;
        }
    }

    class MainClass
    {
        public static void A3()
        {
            Bike b = new MountainBike(true, "acme");
            Console.WriteLine(b.CalculateSize(87));
            ElektroBike c = new ElektroBike();
            Console.WriteLine(c.CalculateSize(34));
        }
    }
}
