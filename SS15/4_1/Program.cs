﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS15._4_1
{
    class Program
    {
        static void MyMethod()
        {
            try
            {
                int[] values = new int[] { 2, 3, 1, 0 };
                Console.Write("1");
                Console.Write(values[0] / values[3]);
                Console.Write("2");
                Console.Write(values[4]);
            }
            catch (IndexOutOfRangeException e) { Console.Write("3"); throw e; }
            catch (DivideByZeroException e) { Console.Write("4"); throw e; }
            finally { Console.Write("5"); }
            Console.Write("6");
        }
        public static void A4()
        {
            Console.Write("A");
            try
            {
                MyMethod();
                Console.Write("B");
            }
            catch (NullReferenceException) { Console.Write("C"); }
            catch (DivideByZeroException) { Console.Write("D"); }
            catch (IndexOutOfRangeException) { Console.Write("E"); }
            catch (Exception) { Console.Write("F"); }
            Console.WriteLine("G");
        }
    }
}
