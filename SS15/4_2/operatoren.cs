﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS15._4_2
{
    delegate bool MyDel();
    class Program
    {
        public static void A42()
        {
            Apfel apfel1 = new Apfel();
            Apfel apfel2 = new Apfel();
            Birne birne1 = new Birne();
            MyDel delVar = ObstMethode;
            if (apfel1 == apfel2) Console.Write("1 ");
            if (apfel1 == birne1) Console.Write("2 ");
            if (apfel1) Console.Write("3 ");
            if (apfel1[delVar]) Console.Write("4 ");
            if (!apfel1) Console.Write("5 ");
        }

        static bool ObstMethode() { Console.Write("6 "); return true; }
    }
    class Apfel
    {
        static int counter = 0;
        public bool this[MyDel d]
        {
            get
            {
                counter++;
                if (d())
                    return true;
                else
                    return false;
            }
        }
        public static bool operator true(Apfel a)
        {
            if ((counter++) > 3)
                return true;
            else
                return false;
        }
        public static bool operator false(Apfel a) { return !true; }
        public static bool operator !(Apfel a) { counter++; return true; }
    }
    class Birne
    {
        public static bool operator ==(Apfel a, Birne b) { return true; }
        public static bool operator !=(Apfel a, Birne b) { return !true; }
    }
}
